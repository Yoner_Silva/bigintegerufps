/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Negocio.BigFactorial_UFPS;

/**
 *
 * @author madar
 * @author yoner silva
 * @author danilo gómez
 * 
 */
public class Test_BigFactorial_UFPS {

    public static void main(String[] args) {
        long horaAnt = System.currentTimeMillis();
        String numeroBig = "100";
        try {
            BigFactorial_UFPS numero = new BigFactorial_UFPS(numeroBig);
            System.out.println("El factorial de:" + numero.toString() + ", es:" + numero.getFactorial().toString());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        long horaDesp = System.currentTimeMillis();
        float tiempo = (horaDesp-horaAnt);
        if(tiempo >= 60000){
            System.out.println("El tiempo de solución es: "+((tiempo)/1000)/60 +" minutos.");
        }else{
            if(tiempo >= 1000)
                System.out.println("El tiempo de solución es: "+(tiempo)/1000 +" segundos.");
            else
                System.out.println("El tiempo de solución es: "+tiempo +" milisegundos.");
            }  
        }
    }