/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 * @author madar
 * @author yoner silva
 * @author danilo gómez
 */
public class BigInteger_UFPS {

    private int miNumero[];
    
    public BigInteger_UFPS() {
    }

    public BigInteger_UFPS(String miNumero){
        this.miNumero = new int[miNumero.length()];
        for (int i = 0; i < miNumero.length(); i++) {
            this.miNumero[i] = Integer.parseInt(String.valueOf(miNumero.charAt(i)));
        }
    }

    public int[] getMiNumero() {
        return miNumero;
    }
    
    public void setMiNumero(int[] miNumero){
        this.miNumero = miNumero;
    }
    
    /**
     * Mutiplica dos enteros BigInteger
     * @param dos
     * @return 
     */
    
    public BigInteger_UFPS multiply(BigInteger_UFPS dos)
    {   
        String cadena = "";
        int suma = 0, resultado = 0;
        for (int i = this.getMiNumero().length-1; i > -1; i--) {
            resultado = this.getMiNumero()[i]*dos.intValue();
            resultado += suma;
            suma = 0;
                cadena += String.valueOf(resultado%10);
                if(resultado >= 10){
                    suma = resultado/10;
                    if(i == 0){
                        if(suma >= 10)
                            cadena += reverse(String.valueOf(suma));
                        else
                            cadena += suma; 
                    }
                }  
        }
        BigInteger_UFPS nuevo = new BigInteger_UFPS(reverse(cadena));
        return nuevo;
    }    
    
    /**
     * Coloca en reversa cada uno de los elementos de una cadena
     * Ejemplo: 12345 --> 54321
     * @param x
     * @return 
    */
    private String reverse(String x){
        String cadena = "";
        for (int i = x.length()-1; i > -1; i--) {
            cadena += x.charAt(i);
        }
        return cadena;
    }
         
    
    /**
     * Retorna la representación entera del BigInteger_UFPS
     * @return un entero
     */
    public int intValue()
    {
        return Integer.parseInt(toString());
    }

    @Override
    public String toString() {
        String cadena = "";
        for (int i : this.miNumero) {
            cadena+= String.valueOf(i);
        }
        return cadena;
    }
    
    
    
    
    
    
    
    
}
